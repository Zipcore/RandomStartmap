#include <sourcemod>

public Plugin myinfo = 
{
	name = "Random Startmap",
	author = "Zipcore",
	description = "Random map on serverstart",
	version = "1.0",
	url = "zipcore#googlemail.com"
}

public void OnPluginStart()
{
	RandomMap();
}

void RandomMap()
{
	// Load config
	Handle hStartmaps = CreateArray(128);
	
	char ConfigPath[256];
	BuildPath(Path_SM, ConfigPath, 256, "configs/random_startmaps.txt");
	
	Handle hFile = OpenFile(ConfigPath, "r");
	char buffer[128];
	if (hFile != null)
	{
		while (ReadFileLine(hFile, buffer, sizeof(buffer))) 
		{
			TrimString(buffer);
			PushArrayString(hStartmaps, buffer);
		}
		
		CloseHandle(hFile);
	}
	
	// Change map
	char sMap[128];
	GetArrayString(hStartmaps, GetRandomInt(0, GetArraySize(hStartmaps)-1), sMap, 128);
	ForceChangeLevel(sMap, "");
}